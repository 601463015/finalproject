import 'package:flutter/material.dart';

class MyStyle {
  Color darkColer = Colors.blue.shade900;
  Color primaryColer = Colors.green;

  SizedBox mySizebox() => const SizedBox(
        width: 8.0,
        height: 16.0,
      );
  //BorderRadius radiusButton() => BorderRadius.circular(50.0,);

  Text showTitle(String title) => Text(
        title,
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.blue.shade900,
            fontWeight: FontWeight.bold),
      );

  Container showLogo() {
    return Container(
      width: 150.0,
      child: Image.asset('images/logo.png'),
    );
  }

  Container showLogoDektop() {
    return Container(
      width: 250.0,
      child: Image.asset('images/logo.png'),
    );
  }

  BoxDecoration myBoxDecorattion(String namePic) {
    return BoxDecoration(
      image: DecorationImage(
          image: AssetImage('images/$namePic'), fit: BoxFit.cover),
    );
  }

  MyStyle();
}
