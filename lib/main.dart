import 'package:aoonjai/screens/home.dart';
import 'package:aoonjai/screens/main_user.dart';
import 'package:aoonjai/screens/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:aoonjai/screens/mainmenu.dart';
import 'package:aoonjai/screens/new_user.dart';
import 'package:aoonjai/screens/type_regis.dart';
// void main() {
//   runApp(MyApp());
// }
main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      title: 'Aoonjai',
      home: MainUser(),
    );
  }
}
