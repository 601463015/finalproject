import 'package:aoonjai/screens/main_sick.dart';
import 'package:aoonjai/screens/main_user.dart';
import 'package:aoonjai/screens/sign_in.dart';
import 'package:aoonjai/screens/sign_up.dart';
import 'package:aoonjai/utility/my_style.dart';
import 'package:aoonjai/utility/normal_dialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<Home> {
  @override
  void initState() {
    super.initState();
    checkPerfernerance();
  }

  Future<Null> checkPerfernerance() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String? choosetype = preferences.getString('Choosetype');
      if (choosetype != null && choosetype.isEmpty) {
        if (choosetype == 'userA') {
          routeToService(MainUser());
        } else if (choosetype == 'userB') {
          routeToService(MainSick());
        } else {
          normalDialog(context, 'Error User Type');
        }
      }
    } catch (e) {}
  }

  void routeToService(Widget myWidget) {
    MaterialPageRoute route = MaterialPageRoute(
      builder: (context) => myWidget,
    );
    Navigator.pushAndRemoveUntil(context, route, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: showDrawer(),
      body: Center(
        child: MyStyle().showLogoDektop(),
      ),
    );
  }

  Drawer showDrawer() => Drawer(
        child: ListView(
          children: <Widget>[
            showHeadDrawer(),
            signInMenu(),
            signUpMenu(),
          ],
        ),
      );

  ListTile signInMenu() {
    return ListTile(
      leading: Icon(Icons.android),
      title: Text('Sign In'),
      onTap: () {
        Navigator.pop(context);
        MaterialPageRoute route =
            MaterialPageRoute(builder: (Value) => SignIn());
        Navigator.push(context, route);
      },
    );
  }

  ListTile signUpMenu() {
    return ListTile(
        leading: Icon(Icons.android),
        title: Text('Sign up'),
        onTap: () {
          Navigator.pop(context);
          MaterialPageRoute route =
              MaterialPageRoute(builder: (Value) => SignUp());
          Navigator.push(context, route);
        });
  }

  UserAccountsDrawerHeader showHeadDrawer() {
    return UserAccountsDrawerHeader(
        decoration: MyStyle().myBoxDecorattion('logo.png'),
        currentAccountPicture: MyStyle().showLogo(),
        accountName: Text('Guest'),
        accountEmail: Text('Please login'));
  }
}
