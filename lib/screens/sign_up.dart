import 'package:aoonjai/utility/my_style.dart';
import 'package:aoonjai/screens/register.dart';
import 'package:aoonjai/utility/normal_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String? chooseType, phonenumber, user, password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
      ),
      body: ListView(
        padding: EdgeInsets.all(30.0),
        children: [
          myLogo(),
          nameForm(),
          MyStyle().mySizebox(),
          userForm(),
          MyStyle().mySizebox(),
          passwordForm(),
          MyStyle().mySizebox(),
          MyStyle().showTitle('ประเภทสมาชิก :'),
          saboRadio(),
          registerButton(),
        ],
      ),
    );
  }

  Widget registerButton() => Container(
      width: 280.0,
      child: ElevatedButton( 
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        // color: MyStyle().darkColer,
        onPressed: () {
          print(
              'phonenumber= $phonenumber, user= $user, password = $password, chooseType= $chooseType');
          if (phonenumber == null || user == null || password == null) {
            print('have space');
            normalDialog(context, 'มีช่องว่าง');
          } else if (chooseType == null) {
            normalDialog(context, 'โปรดเลือกประเภทของผู้สมัคร');
          } else {
            checkUser();
          }

          // Navigator.pop(context);
          // MaterialPageRoute route =
          //     MaterialPageRoute(builder: (Value) => Register());
          // Navigator.push(context, route);
        },
        child: const Text(
          'register',
          style: TextStyle(color: Colors.white),
        ),
      ));

  Future<Null> checkUser() async {
    String url =
        'http://localhost/aoonjai/getUserWhereUser.php?isAdd=true&User=$user';
    try {
      Response response = await Dio().get(url);
      if (response.toString() == 'null') {
        registerThread();
      } else {
        normalDialog(context, 'หมายเลขโทรศัพท์ นี้ $user มีผู้ใช้แล้ว');
      }
    } catch (e) {}
  }

  Future<Null> registerThread() async {
    String url =
        'http://localhost/aoonjai/insertData.php?isAdd=true&PhoneNumber=$phonenumber&User=$user&Password=$password&ChooseType=$chooseType';
    try {
      Response response = await Dio().get(url);
      print('res = $response');

      if (response.toString() == 'true') {
        Navigator.pop(context);
      } else {
        normalDialog(context, 'ไม่สามารถ สมัครได้ กรุณาลองใหม่');
      }
    } catch (e) {}
  }

  Widget userRadio() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 250.0,
            child: Row(
              children: <Widget>[
                Radio(
                  value: 'userA',
                  groupValue: chooseType,
                  onChanged: (String? value) => {
                    setState(() {
                      chooseType = value;
                    })
                  },
                ),
                Text(
                  'ผู้ดูแล',
                  style: TextStyle(color: MyStyle().darkColer),
                )
              ],
            ),
          ),
        ],
      );

  Widget sickRadio() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 250.0,
            child: Row(
              children: <Widget>[
                Radio(
                  value: 'userB',
                  groupValue: chooseType,
                  onChanged: (String? value) => {
                    setState(() {
                      chooseType = value;
                    })
                  },
                ),
                Text(
                  'ผู้ป่วย',
                  style: TextStyle(color: MyStyle().darkColer),
                )
              ],
            ),
          ),
        ],
      );

  Column saboRadio() => Column(
        children: [
          ListTile(
            title: Text('ผู้ดูแล'),
            leading: Radio(
              value: 'userA',
              onChanged: (String? value) {
                setState(() {
                  chooseType = value;
                });
              },
              groupValue: chooseType,
            ),
          ),
          ListTile(
            title: Text('ผู้ป่วย'),
            leading: Radio(
              value: 'userB',
              onChanged: (String? value) {
                setState(() {
                  chooseType = value;
                });
              },
              groupValue: chooseType,
            ),
          ),
        ],
      );

  Widget nameForm() => Container(
        width: 280.0,
        child: TextField(
          onChanged: (value) => phonenumber = value.trim(),
          decoration: InputDecoration(
            prefixIcon:
                Icon(Icons.phone, color: MyStyle().darkColer), // ไม่จำเป็น
            labelStyle: TextStyle(color: MyStyle().darkColer),
            labelText: 'PhoneNumber :',
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().darkColer)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().primaryColer)),
          ),
        ),
      );

  Widget userForm() => Container(
        width: 280.0,
        child: TextField(
          onChanged: (value) => user = value.trim(),
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.account_box,
                color: MyStyle().darkColer), // ไม่จำเป็น
            labelStyle: TextStyle(color: MyStyle().darkColer),
            labelText: 'User :',
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().darkColer)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().primaryColer)),
          ),
        ),
      );

  Widget passwordForm() => Container(
        width: 280.0,
        child: TextField(
          onChanged: (value) => password = value.trim(),
          decoration: InputDecoration(
            prefixIcon:
                Icon(Icons.lock, color: MyStyle().darkColer), // ไม่จำเป็น
            labelStyle: TextStyle(color: MyStyle().darkColer),
            labelText: 'Password :',
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().darkColer)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().primaryColer)),
          ),
        ),
      );

  Widget myLogo() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MyStyle().showLogo(),
        ],
      );
}
