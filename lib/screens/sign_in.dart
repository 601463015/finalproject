import 'dart:convert';
import 'package:aoonjai/model/user_model.dart';
import 'package:aoonjai/screens/main_sick.dart';
import 'package:aoonjai/screens/main_user.dart';
import 'package:aoonjai/utility/my_style.dart';
import 'package:aoonjai/utility/normal_dialog.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SignIn> {
  //Field
  String? user, password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign In'),
      ),
      body: Container(
        color: Colors.green.shade100,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              MyStyle().showLogo(),
              userForm(),
              MyStyle().mySizebox(),
              passwordForm(),
              MyStyle().mySizebox(),
              loginButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget loginButton() => Container(
      width: 280.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        // color: MyStyle().darkColer,
        onPressed: () {
          if (user == null || password == null) {
            normalDialog(context, 'มีช่องว่าง กรุณากรอกให้ครบ');
          } else {
            checkUser();
          }
        },
        child: const Text(
          'Login',
          style: TextStyle(color: Colors.white),
        ),
      ));

  Future<Null> checkUser() async {
    String url =
        'http://localhost/aoonjai/getUserWhereUser.php?isAdd=true&User=$user';
    try {
      Response response = await Dio().get(url);
      print('res = $response');

      var result = json.decode(response.data);
      for (var map in result) {
        UserModel userModel = UserModel.fromJson(map);

        if (password == userModel.Password) {
          String? chooseType = userModel.ChooseType;
          if (chooseType == 'userA') {
            routeToService(MainUser(), userModel);
          } else if (chooseType == 'userB') {
            routeToService(MainSick(), userModel);
          } else {
            normalDialog(context, 'Error');
          }
        } else {
          normalDialog(context, 'Password ไม่ถูกต้อง กรุณากรอกใหม่');
        }
      }
    } catch (e) {}
  }

  Future<Null> routeToService(Widget myWidget, UserModel userModel) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('id', userModel.id!);
    preferences.setString('ChooseType', userModel.ChooseType!);
    preferences.setString('PhoneNumber', userModel.PhoneNumber!);

    MaterialPageRoute route = MaterialPageRoute(
      builder: (context) => myWidget,
    );
    Navigator.pushAndRemoveUntil(context, route, (route) => false);
  }

  Widget userForm() => Container(
        width: 280.0,
        child: TextField(
          onChanged: (value) => user = value.trim(),
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.account_box,
                color: MyStyle().darkColer), // ไม่จำเป็น
            labelStyle: TextStyle(color: MyStyle().darkColer),
            labelText: 'User :',
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().darkColer)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().primaryColer)),
          ),
        ),
      );

  Widget passwordForm() => Container(
        width: 280.0,
        child: TextField(
          onChanged: (value) => password = value.trim(),
          obscureText: true,
          decoration: InputDecoration(
            prefixIcon:
                Icon(Icons.lock, color: MyStyle().darkColer), // ไม่จำเป็น
            labelStyle: TextStyle(color: MyStyle().darkColer),
            labelText: 'Password :',
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().darkColer)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: MyStyle().primaryColer)),
          ),
        ),
      );
}
