import 'package:aoonjai/utility/my_style.dart';
import 'package:aoonjai/utility/singout_process.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainUser extends StatefulWidget {
  @override
  _MainUserState createState() => _MainUserState();
}

class _MainUserState extends State<MainUser> {
  String? nameUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    findUser();
  }

  Future<Null> findUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      nameUser = preferences.getString('PhoneNumber');
    });
  }

  List<Data> lst = [
    Data('การแจ้งเตือน', 1),
    Data('บันทึกความจำ', 2),
    Data('ข่าวสาร', 3),
    Data('กิจวัตรประจำวัน', 4),
    Data('บันทึกอารมณ์', 5),
    Data('ฉุกเฉิน', 6),
  ];

  Widget box(int index) {
    return Container(
      height: 130,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20.0))),
      child: RaisedButton(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [],
            ),
            SizedBox(
              height: 3,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Text('${lst[index].string}'),
                  ],
                ),
              ],
            )
          ],
        ),
        onPressed: () {},
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(nameUser == null ? 'Main User' : '$nameUser login'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () => singOutProcess(context),
          )
        ],
      ),
      body: GridView.builder(
        itemCount: lst.length,
        padding: const EdgeInsets.all(30),
        primary: false,
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 2 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        itemBuilder: (BuildContext context, int index) {
          return box(index);
        },
      ),
      drawer: showDrawer(),
    );
  }

  Text TextBody() {
    return Text(
      "Hello",
      style: TextStyle(fontSize: 30, color: Colors.green),
    );
  }

  Drawer showDrawer() => Drawer(
        child: ListView(
          children: <Widget>[
            showHead(),
          ],
        ),
      );

  UserAccountsDrawerHeader showHead() {
    return UserAccountsDrawerHeader(
        decoration: MyStyle().myBoxDecorattion('logo.png'),
        currentAccountPicture: MyStyle().showLogo(),
        accountName: Text(
          'Name Login',
          style: TextStyle(color: MyStyle().darkColer),
        ),
        accountEmail: Text(
          'login',
          style: TextStyle(color: MyStyle().primaryColer),
        ));
  }
}

class Data {
  Data(this.string, this.i);
  final String string;
  final int i;
}
