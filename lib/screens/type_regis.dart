import 'package:aoonjai/screens/main_sick.dart';
import 'package:aoonjai/screens/main_user.dart';
import 'package:flutter/material.dart';
import 'package:aoonjai/utility/my_style.dart';

class userType extends StatefulWidget {
  const userType({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<userType> {
  String? chooseType, phonenumber, user, password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Sign Up'),
      // ),
      body: ListView(
        padding: EdgeInsets.all(30.0),
        children: [
          myLogo(),
          MyStyle().mySizebox(),
          userButton(),
          MyStyle().mySizebox(),
          sickButton(),
        ],
      ),
    );
  }

  Widget userButton() => Container(
      width: 100.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        // color: MyStyle().darkColer,
        onPressed: () {
          Navigator.pop(context);
          MaterialPageRoute route =
              MaterialPageRoute(builder: (Value) => MainUser());
          Navigator.push(context, route);
        },
        child: const Text(
          'User',
          style: TextStyle(color: Colors.white),
        ),
      ));

  Widget sickButton() => Container(
      width: 100.0,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        // color: MyStyle().darkColer,
        onPressed: () {
          Navigator.pop(context);
          MaterialPageRoute route =
              MaterialPageRoute(builder: (Value) => MainSick());
          Navigator.push(context, route);
        },
        child: const Text(
          'Sick',
          style: TextStyle(color: Colors.white),
        ),
      ));

  Widget myLogo() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MyStyle().showLogo(),
        ],
      );
}
