class UserModel {
  String? id, ChooseType, PhoneNumber, User, Password;

  UserModel(
      {this.id, this.ChooseType, this.PhoneNumber, this.User, this.Password});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ChooseType = json['ChooseType'];
    PhoneNumber = json['PhoneNumber'];
    User = json['User'];
    Password = json['Password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ChooseType'] = this.ChooseType;
    data['PhoneNumber'] = this.PhoneNumber;
    data['User'] = this.User;
    data['Password'] = this.Password;
    return data;
  }
}
